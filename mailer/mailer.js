const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');;

let mailconfig

if(process.env.NODE_ENV==='PRODUCTION'){
    const options ={
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailconfig = sgTransport(options);
}else{
    if(process.env.NODE_ENV === 'STAGING'){
        console.log('XXXXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailconfig = sgTransport(options);
    }else{
        mailconfig = {
            host: 'smtp.ethereal.email',
            port: 587,          
            auth: {
                user: process.env.ETHEREAL_USER,
                pass: process.env.ETHEREAL_PWD
            }
        };
    }
}

module.exports = nodemailer.createTransport(mailconfig);


// const transporter = nodemailer.createTransport({
//     host: 'smtp.ethereal.email',
//     port: 587,
//     auth: {
//         user: 'luisa.schaden60@ethereal.email',
//         pass: 'ZWQPP732yvkJUBDCfR'
//     }
// });

// module.exports = transporter;
