var express = require('express');
var router = express.Router();
var bicicletasController = require('../../controllers/api/bicicletasControllerAPI');

router.get('/', bicicletasController.bicicletas_list);
router.post('/create', bicicletasController.bicicleta_create);
router.delete('/delete', bicicletasController.bicicleta_delete);
router.put('/update', bicicletasController.bicicleta_update);

module.exports = router;