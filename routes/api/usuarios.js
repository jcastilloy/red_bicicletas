var express = require('express');
var router = express.Router();
var usuariosController = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuariosController.usuario_list);
router.post('/create', usuariosController.usuario_create);
router.delete('/reserva', usuariosController.usuario_reservar);


module.exports = router;