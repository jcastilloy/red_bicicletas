var map = L.map('main_map').setView([14.568589, -90.578589], 10);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// L.marker([14.513579,-90.598579]).addTo(map);
// L.marker([14.599865,-90.558655]).addTo(map);
// L.marker([14.572357,-90.508445]).addTo(map);

$.ajax({
    type:"GET",
    dataType: "json",
    url: "api/bicicletas",
    success: function (result) { 
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            console.log(bici.ubicacion);
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
});
