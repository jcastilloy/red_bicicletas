var mongoose = require('mongoose');
var Bicicleta = require("../../models/bicicleta");
var server = require("../../bin/www");
var request = require('request');
const { head } = require('request');
//const { response } = require("express");

var base_url ="http://localhost:3000/api/bicicletas";

describe('Bicicleta API', () =>{
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
        
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe("GET BICICLETAS /", () =>{
        it('Status 200', (done) => {
            var header = {'Content-type' : 'application/json'};
            var aBici = '{"ocd": 10, "color": "rojo", "modelo": "urbana", "lat": 14.513579, "lng": -90.598579}';
            request.post({
                headers: header,
                url: base_url + '/create',
                body: aBici
            },function(err, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(14.513579);
                expect(bici.ubicacion[1]).toBe(-90.598579);
                done();
            });

        });
    });
});





// beforeEach(() =>{Bicicleta.allBicis=[];});


// describe('Bicicleta API',() =>{
//     describe('GET BICICLETAS /', () =>{
//         it('Status 200', done =>{
//             expect(Bicicleta.allBicis.length).toBe(0);

//             var a = new Bicicleta (1, 'rojo', 'urbana', [14.513579,-90.598579]);
//             Bicicleta.add(a);

//             request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
//                 expect(response.statusCode).toBe(200);
//                 done();
//             });
//         });
//     });

//     describe('POST BICICLETAS /create', () => {
//         it('Status 200', done =>{
//             var header = {'Content-Type' : 'application/json'};
//             var aBici = '{"id":10, "color": "rojo", "modelo": "urbano", "lat": 14.372357, "lng":-90.308445}';
//             request.post({
//                     headers: header,
//                     url: "http://localhost:3000/api/bicicletas/create",
//                     body: aBici
//                 }, function(error, response, body){
//                     expect(response.statusCode).toBe(200);
//                     expect(Bicicleta.findById(10).color).toBe("rojo");
//                     done();
//                 }
//             );
//         });
//     });

//     describe('DELETE BICICLETAS /delete', () =>{
//         it('Status code 204', done =>{
//             var a = new Bicicleta (1, 'rojo', 'urbana', [14.513579,-90.598579]);
//             var b = new Bicicleta (2, 'verde', 'montaña', [14.513579,-90.598579]);
//             Bicicleta.add(a);
//             Bicicleta.add(a);
//             var header = {'Content-Type' : 'application/json'};
//             var aBici = '{"id":1}';
//             request.delete({
//                     headers: header,
//                     url: "http://localhost:3000/api/bicicletas/delete",
//                     body: aBici
//                 }, function(error, response, body){
//                     expect(response.statusCode).toBe(204);
//                     expect(Bicicleta.allBicis.length).toBe(1);
//                     done();
//                 }
//             );
//         });
//     });

// });